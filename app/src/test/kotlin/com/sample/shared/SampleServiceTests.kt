package com.sample.shared

import com.sample.shared.http.models.GoogleResponseModel
import com.sample.shared.services.ISampleService
import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.test.assertEquals

class SampleServiceTests {
    class SampleServiceMock : ISampleService {
        override suspend fun requestFromApi(): GoogleResponseModel {
            return GoogleResponseModel("fakeKind", "fakeServicePath", false, "fakeName")
        }

    }

    @Test
    fun test_sample_service_should_return_result() {
        val mock = SampleServiceMock()

        val expectedKind = "fakeKind"
        val expectedServicePath = "fakeServicePath"
        val expectedVersionModule = false
        val expectedName = "fakeName"

        runBlocking {
            val result = mock.requestFromApi()

            assertEquals(expectedKind, result.kind)
            assertEquals(expectedServicePath, result.servicePath)
            assertEquals(expectedVersionModule, result.versionModule)
            assertEquals(expectedName, result.name)
        }
    }
}


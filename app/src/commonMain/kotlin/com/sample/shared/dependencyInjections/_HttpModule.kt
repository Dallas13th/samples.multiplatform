package com.sample.shared.dependencyInjections

import com.sample.shared.http.*
import kotlinx.serialization.json.Json
import org.kodein.di.DI
import org.kodein.di.bind
import org.kodein.di.provider
import org.kodein.di.singleton

val httpModule = DI {
    bind<Json>() with singleton { CustomJsonSerializer().serializer }
    bind<IHttpClient>() with provider { HttpClient() }
}
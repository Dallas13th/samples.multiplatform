package com.sample.shared.dependencyInjections

import com.sample.shared.services.ISampleService
import com.sample.shared.services.SampleService
import org.kodein.di.*

val serviceModule = DI {
    bind<ISampleService>() with provider { SampleService() }
}
package com.sample.shared.coroutines

import kotlinx.coroutines.CancellableContinuation
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlinx.coroutines.withTimeout

expect class CustomMainScope() : CoroutineScope

val customCoroutineScope: CustomMainScope
    get() = CustomMainScope()

suspend fun <T> timeoutCoroutine(timeout: Long = 30000, block: (CancellableContinuation<T>) -> Unit): T {
    return withTimeout(timeout) {
        suspendCancellableCoroutine(block)
    }
}

suspend fun <T> withDefaultTimeout(timeout: Long = 30000, block: suspend CoroutineScope.() -> T): T {
    return withTimeout(timeout, block)
}
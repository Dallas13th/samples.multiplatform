package com.sample.shared.services

import com.sample.shared.dependencyInjections.httpModule
import com.sample.shared.http.IHttpClient
import com.sample.shared.http.models.GoogleResponseModel
import org.kodein.di.instance
import kotlin.coroutines.cancellation.CancellationException

interface ISampleService {
    @Throws(CancellationException::class)
    suspend fun requestFromApi(): GoogleResponseModel
}

class SampleService: ISampleService {
    private val httpClient: IHttpClient by httpModule.instance()

    override suspend fun requestFromApi(): GoogleResponseModel {
        return httpClient.testGoogleApiGet()
    }
}

package com.sample.shared.http

import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule

class CustomJsonSerializer {
    val serializer = Json {
        encodeDefaults = true
        ignoreUnknownKeys = true
        isLenient = true
        coerceInputValues = true

        serializersModule = SerializersModule {
            //contextual serializer of data types
        }
    }
}

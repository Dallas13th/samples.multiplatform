package com.sample.shared.http.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class GoogleResponseModel (
    val kind: String,
    val servicePath: String,

    @SerialName("version_module")
    val versionModule: Boolean,

    val name: String
) {
    override fun toString(): String {
        return "Class GoogleResponseModel: KIND=$kind ServicePath=$servicePath VersionModule=$versionModule Name: $name"
    }
}
package com.sample.shared.http

import com.sample.shared.dependencyInjections.httpModule
import com.sample.shared.http.models.GoogleResponseModel
import com.sample.shared.tools.LogWriter
import io.ktor.client.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.features.logging.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.serialization.json.Json
import org.kodein.di.instance

interface IHttpClient {
    suspend fun testGoogleApiGet(): GoogleResponseModel
}

class HttpClient: IHttpClient {
    private val jsonSerializer: Json by httpModule.instance()

    // Lazy to avoid InvalidMutabilityException: mutation attempt of frozen kotlin.collections.HashMap from DI
    private val httpClient by lazy {
        HttpClient {
            install(JsonFeature) {
                serializer = KotlinxSerializer(jsonSerializer)
            }

            install(Logging) {
                logger = HttpLogger()
                level = LogLevel.INFO // 'BODY' and 'ALL' crashes app when sending multipart form
            }

            expectSuccess = true // trigger exception if status code is not success(>=300)
            followRedirects = false
        }
    }

    init {
        // Trigger lazy property, catching InvalidMutabilityException: Frozen during lazy computation
        // https://youtrack.jetbrains.com/issue/KTOR-1087
        try {
            jsonSerializer
            httpClient
        } catch (e: Exception) {
            //ignore
        }
    }

    override suspend fun testGoogleApiGet(): GoogleResponseModel {
        return httpClient.get("https://searchconsole.googleapis.com/\$discovery/rest?version=v1")
    }

    private fun HttpRequestBuilder.setJsonContentType() = header(HttpHeaders.ContentType, ContentType.Application.Json)
}

class HttpLogger : Logger {
    override fun log(message: String) {
        LogWriter.writeLine("HTTP_LOG:$message")
    }
}
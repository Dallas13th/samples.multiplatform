package com.sample.shared.databases

import com.sample.databases.SampleDatabase
import com.squareup.sqldelight.db.SqlDriver

interface IDbConnectionFactory {
    fun getDriver(): SqlDriver
    fun createDbConnection(): SampleDatabase
    fun migrateDatabaseSchema()
}

expect class DbConnectionFactory : IDbConnectionFactory {
    override fun getDriver(): SqlDriver
    override fun createDbConnection(): SampleDatabase
    override fun migrateDatabaseSchema()
}

object DatabaseHelper {
    const val databaseFileName = "SampleDatabase.sqlite"
    private const val targetDbVersion = 1

    fun migrateDatabaseSchema(driver: SqlDriver) {
        SampleDatabase.Schema.migrate(driver, 0, targetDbVersion)
    }

    fun createDbConnection(driver: SqlDriver) = SampleDatabase(driver)
}
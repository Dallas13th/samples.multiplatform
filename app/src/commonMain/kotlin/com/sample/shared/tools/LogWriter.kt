package com.sample.shared.tools

expect object LogWriter {
    fun writeLine(message: String)
}
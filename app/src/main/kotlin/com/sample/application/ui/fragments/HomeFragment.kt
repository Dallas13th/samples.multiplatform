package com.sample.application.ui.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.sample.shared.R
import com.sample.shared.coroutines.customCoroutineScope
import com.sample.shared.dependencyInjections.httpModule
import com.sample.shared.services.ISampleService
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.kodein.di.instance

class HomeFragment: Fragment() {

    private val sampleService: ISampleService by httpModule.instance()
    private lateinit var requestStatusView: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_home, container, false)
        requestStatusView = view.requestStatusLabel

        view.goToSecondViewButton.setOnClickListener {
            onNextViewClick()
        }

        view.webRequestButton.setOnClickListener {
            onWebRequestClick()
        }

        return view
    }

    private fun onNextViewClick() {
        findNavController().navigate(R.id.navigationSecondViewAction)
    }

    @SuppressLint("SetTextI18n")
    private fun onWebRequestClick() {
        requestStatusView.text = "Waiting for response..."
        customCoroutineScope.launch {
            val result = sampleService.requestFromApi()

            GlobalScope.launch(Dispatchers.Main) {
                requestStatusView.text = "RESPONSE: ${result}"
            }
        }
    }
}
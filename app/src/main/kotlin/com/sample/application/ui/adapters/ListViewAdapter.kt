package com.sample.application.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.sample.shared.R
import tables.SampleTable

class ListViewAdapter : BaseAdapter {

    var dataItems = mutableListOf<SampleTable>()
    private var layoutInflater: LayoutInflater

    constructor(context: Context) : super() {
        layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        var view: View? = convertView

        if (view == null) {
            view = layoutInflater.inflate(R.layout.list_item_sample, null)
        }

        val nameLabel = view!!.findViewById<TextView>(R.id.nameLabel)

        val dataItem = dataItems[position]
        nameLabel.text = dataItem.stringVal

        return view
    }

    override fun getItem(position: Int): SampleTable {
        return dataItems[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return dataItems.size
    }
}
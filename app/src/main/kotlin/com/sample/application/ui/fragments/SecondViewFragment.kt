package com.sample.application.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ListView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.sample.application.dependencyInjections.sqliteModule
import com.sample.application.ui.adapters.ListViewAdapter
import com.sample.databases.SampleDatabase
import com.sample.shared.R
import com.sample.shared.databases.IDbConnectionFactory
import kotlinx.android.synthetic.main.fragment_second_view.view.*
import org.kodein.di.factory

class SecondViewFragment: Fragment() {
    private lateinit var listView: ListView
    private lateinit var newItemEditText: EditText
    private lateinit var dbContext: SampleDatabase
    private lateinit var listViewAdapter: ListViewAdapter

    private val dbConnectionFactory: (Context) -> IDbConnectionFactory by sqliteModule.factory()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_second_view, container, false)

        dbContext = dbConnectionFactory(requireContext()).createDbConnection()

        listViewAdapter = ListViewAdapter(requireContext())

        listView = view.sampleListView
        listView.adapter = listViewAdapter

        newItemEditText = view.newItemEditText

        view.backButton.setOnClickListener {
            popView()
        }

        view.addButton.setOnClickListener {
            onAddClick()
        }

        view.deleteButton.setOnClickListener {
            deleteAll()
        }

        updateListView()

        return view
    }

    private fun popView() {
        findNavController().popBackStack()
    }

    private fun updateListView() {
        val queryResult = dbContext.sampleTableQueries.getAll()
                .executeAsList()

        listViewAdapter.dataItems = queryResult.toMutableList()
        listViewAdapter.notifyDataSetChanged()
    }

    private fun onAddClick() {
        val text = newItemEditText.text.toString()
        if (text.isBlank()) return

        dbContext.sampleTableQueries.insertOrReplace(true, text)
        newItemEditText.setText("")
        updateListView()
    }

    private fun deleteAll() {
        dbContext.sampleTableQueries.deleteAll()
        updateListView()
    }
}
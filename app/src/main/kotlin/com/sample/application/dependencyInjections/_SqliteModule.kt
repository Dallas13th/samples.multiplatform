package com.sample.application.dependencyInjections

import android.content.Context
import com.sample.application.MainActivity
import com.sample.shared.databases.DbConnectionFactory
import com.sample.shared.databases.IDbConnectionFactory
import org.kodein.di.*

val sqliteModule = DI {
    bind<IDbConnectionFactory>() with factory { ctx:Context -> DbConnectionFactory(ctx) }
}
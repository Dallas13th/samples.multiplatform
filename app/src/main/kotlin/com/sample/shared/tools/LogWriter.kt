package com.sample.shared.tools

actual object LogWriter {
    actual fun writeLine(message: String) {
        println(message)
    }
}
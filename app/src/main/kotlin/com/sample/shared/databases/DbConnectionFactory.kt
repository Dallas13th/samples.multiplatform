package com.sample.shared.databases

import android.content.Context
import com.sample.databases.SampleDatabase
import com.squareup.sqldelight.android.AndroidSqliteDriver
import com.squareup.sqldelight.db.SqlDriver

actual class DbConnectionFactory(context: Context) : IDbConnectionFactory {
    private val context: Context = context
    private val driver: AndroidSqliteDriver

    init {
        driver = AndroidSqliteDriver(SampleDatabase.Schema, context, DatabaseHelper.databaseFileName)
    }

    actual override fun getDriver(): SqlDriver {
        return driver
    }

    actual override fun createDbConnection(): SampleDatabase {
        return DatabaseHelper.createDbConnection(driver)
    }

    actual override fun migrateDatabaseSchema() {
        DatabaseHelper.migrateDatabaseSchema(driver)
    }
}
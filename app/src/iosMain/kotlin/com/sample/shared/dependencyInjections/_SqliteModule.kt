package com.sample.shared.dependencyInjections

import com.sample.shared.databases.DbConnectionFactory
import com.sample.shared.databases.IDbConnectionFactory
import org.kodein.di.DI
import org.kodein.di.bind
import org.kodein.di.provider

val sqliteModule = DI {
    bind<IDbConnectionFactory>() with provider { DbConnectionFactory() }
}
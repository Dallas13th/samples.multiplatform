package com.sample.shared.dependencyInjections

import com.sample.shared.databases.IDbConnectionFactory
import org.kodein.di.instance

class DbConnectionInjection {
    val dbConnectionFactory: IDbConnectionFactory by sqliteModule.instance()
}
package com.sample.shared.dependencyInjections

import com.sample.shared.services.ISampleService
import org.kodein.di.instance

class ServiceInjection {
    val sampleService: ISampleService by serviceModule.instance()
}
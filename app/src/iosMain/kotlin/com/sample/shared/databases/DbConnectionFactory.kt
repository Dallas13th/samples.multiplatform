package com.sample.shared.databases

import com.sample.databases.SampleDatabase
import com.squareup.sqldelight.db.SqlDriver
import com.squareup.sqldelight.drivers.native.NativeSqliteDriver

actual class DbConnectionFactory : IDbConnectionFactory {
    private val driver: NativeSqliteDriver = NativeSqliteDriver(SampleDatabase.Schema, DatabaseHelper.databaseFileName)

    actual override fun getDriver(): SqlDriver {
        return driver
    }

    actual override fun createDbConnection(): SampleDatabase {
        return DatabaseHelper.createDbConnection(driver)
    }

    actual override fun migrateDatabaseSchema() {
        DatabaseHelper.migrateDatabaseSchema(driver)
    }
}
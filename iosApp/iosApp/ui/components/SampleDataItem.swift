//
//  SampleDataItem.swift
//  iosApp
//
//  Created by admin on 19.11.2020.
//

import SwiftUI

struct SampleDataItem: Identifiable {
    var id: UUID = UUID()
    
    var name: String
}

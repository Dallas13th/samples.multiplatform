//
//  SecondView.swift
//  iosApp
//
//  Created by admin on 19.11.2020.
//

import SwiftUI
import app

struct SecondView: View {
    @Environment(\.presentationMode)
    var presentation
    
    @State
    var newItemNameText: String = ""
    
    @State
    var dataSource: [SampleDataItem] = []
    
    var body: some View {
        VStack {
            
            Button(action: self.goBack) {
                Text("Go Back")
            }
            
            ScrollView {
                VStack {
                    ForEach(self.dataSource) { dataItem in
                        Text(dataItem.name)
                    }
                }
            }
            
            TextField("Enter Name", text: self.$newItemNameText)
            
            Button(action: self.onAddItemClick) {
                Text("Add Item")
            }
            
            Button(action: self.deleteAll) {
                Text("Delete All")
            }
        }.navigationBarTitle("Second view")
        .onAppear {
            self.reloadSource()
        }
    }
    
    private func goBack() {
        self.presentation.wrappedValue.dismiss()
    }
    
    private func reloadSource() {
        let dataItems = DbConnectionFactory()
            .createDbConnection()
            .sampleTableQueries
            .getAll(mapper: { _, _, name in SampleDataItem(name: name) })
            .executeAsList()
            .map { item in item as! SampleDataItem }
        
        self.dataSource = dataItems
    }
    
    private func onAddItemClick() {
        if (self.newItemNameText.isEmpty) { return }
        
        let dbContext = DbConnectionFactory()
            .createDbConnection()
        
        dbContext.sampleTableQueries.insertOrReplace(boolVal: false, stringVal: self.newItemNameText)
        
        self.newItemNameText = ""
        
        self.reloadSource()
    }
    
    private func deleteAll() {
        let dbContext = DbConnectionFactory()
            .createDbConnection()
        
        dbContext.sampleTableQueries.deleteAll()
        
        self.reloadSource()
    }
}

struct SecondView_Previews: PreviewProvider {
    static var previews: some View {
        SecondView()
    }
}

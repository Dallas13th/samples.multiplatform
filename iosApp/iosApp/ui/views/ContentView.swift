//
//  ContentView.swift
//  iosApp
//
//  Created by admin on 19.11.2020.
//

import SwiftUI
import app

struct ContentView: View {
    @State
    var showSecondView = false
    
    @State
    var httpStatusString: String = "Press to request"
    
    var body: some View {
        NavigationView {
            VStack {
                NavigationLink(destination: SecondView(),
                               isActive: self.$showSecondView) {
                    EmptyView()
                }
                
                Spacer()
                
                Button(action: {
                    self.showSecondView.toggle()
                }) {
                    Text("Show second view")
                }
                
                Spacer()
                
                Text(httpStatusString)
                                
                Button(action: self.performRequest) {
                    Text("Perform request")
                }
                
                Spacer()
                
            }.navigationBarTitle(Text(Proxy().proxyHello()))
        }
    }
    
    private func performRequest() {
        self.httpStatusString = "waiting for response..."
        ServiceInjection().sampleService.requestFromApi { result, error in
            DispatchQueue.main.async {
                if (error != nil) {
                    print("An error has occurred: \(error!)")
                    return
                }
                
                guard let model = result else { return }
                
                self.httpStatusString = String(reflecting: model)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
